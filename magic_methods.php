<?php

class Person
{

    public $name = "Test Name";
    public $phone = "Test Phone";
    public $dateOfBirth = "01-01-17";

    public static $message;

    public static function doFrequently(){

        echo "I'm doing it frequently"."<br>";

    }

    public function __construct()
    {
        echo "I'm inside the" . __METHOD__ . "<br>";
    }

    public function __destruct()
    {
        echo "I'm dying, I'm inside the" . __METHOD__ . "<br>";
    }



//class Person1{
//
//    public $name;
//
//    public function __construct()
//    {
//        echo "I'm inside the".__METHOD__."<br>";
//    }
//
//    public function __destruct()
//    {
//        echo "I'm dying, I'm inside the".__METHOD__."<br>";
//    }
//
//}

    public function __call($name, $arguments)
    {
        echo "I'm inside the" . __METHOD__ . "<br>";

        echo "name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }

    public static function __callStatic($name, $arguments)
    {
        echo "I'm inside the static" . __METHOD__ . "<br>";

        echo "wrong static name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }

    public function __set($name, $value)
    {
        echo __METHOD__."Wrong Property name = $name<br>";
        echo "Value tried to set to that wrong property = $value<br>";

    }

    public function __get($name)
    {
        echo __METHOD__."Wrong Property name = $name<br>";

    }

    public function __isset($name)
    {
        echo __METHOD__."Wrong Property name = $name<br>";
    }

    public function __unset($name)
    {
        echo __METHOD__."Wrong Property name = $name<br>";
    }

    public function __sleep()
    {
       return array("name", "phone");
    }

    public function __wakeup($name)
    {
        // do any task here.....
    }

    public function __toString()
    {
        return "Are you crazy? I'm just an object.<br>";
    }

    public function __invoke()
    {
        echo "I'm an object but you are trying to call me as a function! <br>";
    }


} // end of person and person 1 class

Person::$message = "static value"; // static property setting test
Person::$message . "<br>"; // static property reading test


//Person::doFrequently(); // static method call korar style
Person::doFrequentliiii("wrong static para 1","wrong static para 2"); // __callStatic


$obj = new person(); // __construct

//unset($obj);
//
//$obj = new person1();

$obj->habijabi("para_habi1","para_jabi1");  // __call()

$obj->address = "5/2 Gol Pahar Mor, Chittagong";  // __set()

echo $obj->address; // __get()


if(isset($obj->naiProperty)){    // __isset()

}

unset($obj->naiProperty);       // __unset()


$myVar = serialize($obj);
echo "<pre>";
var_dump($myVar);
echo "</pre>";

echo "<pre>";
var_dump(unserialize($myVar));
echo "</pre>";


echo $obj; // __toString


$obj();